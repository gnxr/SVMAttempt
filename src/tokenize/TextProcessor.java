/*
 * (C) 2018 pandarou
 * Licensed under the Apache License, Version 2.0
 * Read it there: http://www.apache.org/licenses/LICENSE-2.0
 */
package tokenize;

import java.util.ArrayList;

/**
 * A {@link StringOperator} chain.
 * @author pandarou
 */
public class TextProcessor extends ArrayList<StringOperator>
        implements StringOperator {
    private static final long serialVersionUID = 1L;

    @Override
    public String apply(String in) {
        String out = in;
        for (StringOperator op : this) {
            out = op.apply(out);
        }
        return out;
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
