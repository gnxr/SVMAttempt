/*
 * (C) 2018 pandarou
 * Licensed under the Apache License, Version 2.0
 * Read it there: http://www.apache.org/licenses/LICENSE-2.0
 */
package tokenize;

/**
 * Simple {@link StringOperator} to remove CamelCase.
 * Also removes most special characters.<br>
 * Sample input/output:<br>
 * {@code "#UnSuperDVD.txt"}
 * {@code " Un Super DVD txt"}
 *
 * @author pandarou
 */
public class CamelCaseRemover implements StringOperator {
    private static int getState(int c) {
        if (Character.isDigit(c)) {
            return 'd';
        }
        if (Character.isLowerCase(c)) {
            return 'l';
        }
        if (Character.isUpperCase(c)) {
            return 'u';
        }
        return Character.isAlphabetic(c) ? 'a' : 'o';
    }

    @Override
    public String apply(String str) {
        StringBuilder sb = new StringBuilder(str.length());
        int state = 'o';

        for (int i = 0; i < str.length(); ++i) {
            int c = str.codePointAt(i);
            int prev = state;
            int next = (i < str.length() - 1)
                    ? getState(str.codePointAt(i + 1))
                    : 'o';
            state = getState(c);
            switch (state) {
                case 'd':
                case 'a':
                case 'l':
                    sb.appendCodePoint(c);
                    break;
                case 'u':
                    if (prev == 'l') {
                        sb.append(' ');
                    }
                    if (next == 'l') {
                        sb.append(' ');
                    }
                    sb.appendCodePoint(c);
                    break;
                case 'o':
                    sb.appendCodePoint(' ');
                    break;
            }
        }
        return sb.toString();
    }
}
